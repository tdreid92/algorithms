/**
Time: O(nm)
Space: O(nm)
Not sure how else to beat these complexities. Even sorting still forces me to evaluate nearly every candidate.
**/
public class Solution {
    public List<List<Integer>> findBestMemUse(List<List<Integer>> fores, List<List<Integer>> backs, int K) {
        int bestDiff = Integer.MAX_VALUE;
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        for (List<Integer> fore : fores) {
            for (List<Integer>  back: backs) {
                int diff = (K - (fore.get(1) + back.get(1)));
                if (diff >= 0 && diff <= bestDiff) {
                    List<Integer> candidate = new ArrayList<Integer>();
                    candidate.add(fore.get(0));
                    candidate.add(back.get(0));
                    if (diff < bestDiff) {
                        bestDiff = diff;
                        result.clear();
                    }
                    result.add(candidate);
                }
            }
        }
        return result;
    }
}