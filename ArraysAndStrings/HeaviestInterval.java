/**
Find heaviest interval. Intervals may overlap. [1, 5, 5], [3, 6, 10], [7, 9, 3], and [4, 8, 4] the output is [4, 5, 19]
SOLVED BY SELF WOOHO.
Time: O(nlogn)
Space: O(1)
nlogn because of sorting by first vaue in intervals. If intervals overlap, build the new, heavier intersecting
interval. If no intersection, but the interval is still heavier than the current heaviest interval, replace it.
If this were sorted input, complexity would be O(n)
**/
class Solution {

  public int[] biggestInterval(int[][] intervals) {
    Arrays.sort(intervals, new Comparator<int[]>() {           
      @Override              
      public int compare( int[] o1, int[] o2) {
        return o1[0] - o2[0]; 
      }
    }); 
    int[] ans = new int[3];
    for (int i = 0; i < intervals.length; i++) {
      int start = Math.max(ans[0], intervals[i][0]);
      int end = Math.min(ans[1], intervals[i][1]);
      if (start <= end) ans = new int[]{start, end, ans[2] + intervals[i][2]};
      else
        if (intervals[i][2] > ans[2]) ans = intervals[i];
    }
    return ans;
  }
}