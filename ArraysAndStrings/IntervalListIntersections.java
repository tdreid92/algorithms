/**
Given two lists of closed intervals, each list of intervals is pairwise disjoint and in sorted order.

Return the intersection of these two interval lists.

Example 1:
Input: A = [[0,2],[5,10],[13,23],[24,25]], B = [[1,5],[8,12],[15,24],[25,26]]
Output: [[1,2],[5,5],[8,10],[15,23],[24,24],[25,25]]

Time: O(m + n) 
space: (m + n)
Credit to I93 for this solution. Mine was utter crap! I need to remember the start <= end is critical for many of these
interval algorithms. Also use Math.min / Math.max more.
**/
class Solution {
    public int[][] intervalIntersection(int[][] A, int[][] B) {
        List<int[]> ans = new ArrayList<>();
        int i = 0, j = 0;
        while(i < A.length && j< B.length){
            int start = Math.max(A[i][0], B[j][0]);
            int end = Math.min(A[i][1], B[j][1]);
            //System.out.println("start: " + start + " end: " + end);
            if(start <= end) ans.add(new int[]{start, end});
            if(A[i][1]>B[j][1]) j++;
            else i++;
        }
        
        int[][] res = new int[ans.size()][2];
        i = 0;
        for(int[] pair: ans){
            res[i++] = pair;
        }
        
        return res;
    }