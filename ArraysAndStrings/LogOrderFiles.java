/**
You have an array of logs.  Each log is a space delimited string of words.

For each log, the first word in each log is an alphanumeric identifier.  Then, either:

Each word after the identifier will consist only of lowercase letters, or;
Each word after the identifier will consist only of digits.
We will call these two varieties of logs letter-logs and digit-logs.  It is guaranteed that each log has at least one word after its identifier.

Reorder the logs so that all of the letter-logs come before any digit-log.  The letter-logs are ordered lexicographically ignoring identifier, with the identifier used in case of ties.  The digit-logs should be put in their original order.

Return the final order of the logs.

 

Example 1:

Input: ["a1 9 2 3 1","g1 act car","zo4 4 7","ab1 off key dog","a8 act zoo"]
Output: ["g1 act car","a8 act zoo","ab1 off key dog","a1 9 2 3 1","zo4 4 7"]

Time: O(nlogn)
Space: O(1)

This solution is inspired by Zzz_. I initially made another algorithm which evaluated the each log by log-type,
then merged arrays. This is more efficient and uses constant space (I believe!)
**/
class Solution {
    public String[] reorderLogFiles(String[] logs) {
        Arrays.sort(logs, new Comparator<String>() {
            @Override
            public int compare(String str1, String str2) {
                String[] s1 = str1.split(" ");
                String[] s2 = str2.split(" ");
                
                if (Character.isDigit(s1[1].charAt(0)) && Character.isDigit(s2[1].charAt(0))) return 0;
                if (Character.isLetter(s1[1].charAt(0)) && Character.isDigit(s2[1].charAt(0))) return -1;
                if (Character.isDigit(s1[1].charAt(0)) && Character.isLetter(s2[1].charAt(0))) return 1;
                
                String log1 = str1.substring(str1.indexOf(" ") + 1);
                String log2 = str2.substring(str2.indexOf(" ") + 1);
                if (log1.equals(log2)) return s1[0].compareTo(s2[0]);
                return log1.compareTo(log2);
                
            }
        });
        return logs;
    }
}