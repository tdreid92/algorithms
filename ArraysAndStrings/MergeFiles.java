/**
This merges files starting from the smallest to biggest. Files are represeneted by integers and the time it takes to
merge is represented by the addition of two integers from nums.
Time: O(n)
Space: O(1)
**/
public class Solution {
    public static int mergeFiles(int[] nums) {
        if (nums.length == 0) return 0;
        if (nums.length == 1) return nums[0];
        Arrays.sort(nums);
        int time = 0;
        for (int i = 1; i < nums.length; i++) {
            nums[i] = nums[i] + nums[i - 1];
            time += nums[i];
        }
        return time;
    }
}