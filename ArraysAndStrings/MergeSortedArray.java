/**Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.

Note:

The number of elements initialized in nums1 and nums2 are m and n respectively.
You may assume that nums1 has enough space (size that is greater or equal to m + n) to hold additional elements from nums2.
Example:

Input:
nums1 = [1,2,3,0,0,0], m = 3
nums2 = [2,5,6],       n = 3

Output: [1,2,2,3,5,6]
Time: O(n + m)
Space O(1)
Repositions nonzero values in nums1 for clean while loop (Allowing constant space solution). Adds lowest value 
from either array in while loop, then increments index per array nums1 or nums2 respectively until both indexes
are outside of their boundary.
**/
class Solution {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        if (n == 0) return;
        for (int i = nums1.length - 1; i >= nums1.length - m ; i--) {
            nums1[i] = nums1[i - (nums1.length - m)];
            nums1[i - (nums1.length - m)] = 0;
        }
        int i = 0, j = 0, ind = 0;
        while (i < m || j < n) {
            if (i < m && j < n) {
                nums1[ind++] = nums1[i + (nums1.length - m)] < nums2[j] ? nums1[i++ + (nums1.length - m)] : nums2[j++]; 
            } else {
                nums1[ind++] = i >= m ? nums2[j++] : nums1[i++ + (nums1.length - m)]; 
            }
        }
    }
}