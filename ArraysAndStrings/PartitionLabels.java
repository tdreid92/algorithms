/**
A string S of lowercase letters is given. We want to partition this string into as many parts as possible so that each letter appears in at most one part, and return a list of integers representing the size of these parts.

Example 1:
Input: S = "ababcbacadefegdehijhklij"
Output: [9,7,8]
Explanation:
The partition is "ababcbaca", "defegde", "hijhklij".
This is a partition so that each letter appears in at most one part.
A partition like "ababcbacadefegde", "hijhklij" is incorrect, because it splits S into less parts.

Time: O(n)
Space: O(n)
Solved by self. Make a map for the last position of each char. Then make a second pass that iterates as long as the 
length of the string. This makes it O(n) complexity. The second pass will increment bestLast if the latest substring
has a value that exceeds the last index of the first value. This substring length is increased upon each positive find
that is greater than the last.

**/
class Solution {
    public List<Integer> partitionLabels(String S) {
        List<Integer> answer = new ArrayList<Integer>();
        if (S.length() == 0) return answer;
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        
        for (int i = 0; i < S.length(); i++) {
            map.put(S.charAt(i), i);
        }
        for (int i = 0; i < S.length(); i++) {
            int bestLast = map.get(S.charAt(i));
            int j = 0;
            while (j < bestLast) {
                bestLast = Math.max(bestLast, map.get(S.charAt(j)));
                j++;
            }
            answer.add(bestLast + 1 - i);
            i = bestLast;
        }
        return answer;
    }
}