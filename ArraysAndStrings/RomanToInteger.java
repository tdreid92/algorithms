
Written by self. Extremely time and space efficient. Traverses through array backwards. Checks if last char was a L
or C if current char is X, or if last char is D or M if current char is C. This increments or decrements accordingly.
Technically space is O(n) since I made an array.
**/
class Solution {  
    public int romanToInt(String s) {

        char[] arr = s.toCharArray();
        int total = 0;
        
        char lastChar = 'c';
        for (int i = arr.length - 1; i >= 0; i--) {
            if (arr[i] == 'I') total += lastChar == 'V' || lastChar == 'X' ? -1 : 1; 
            else if (arr[i] == 'V') total += 5;
            else if (arr[i] == 'X') total += lastChar == 'L' || lastChar == 'C' ? -10 : 10; 
            else if (arr[i] == 'L') total += 50;
            else if (arr[i] == 'C') total += lastChar == 'D' || lastChar == 'M' ? -100 : 100; 
            else if (arr[i] == 'D') total += 500;
            else if (arr[i] == 'M') total += 1000;
            lastChar = arr[i];
        }
        return total;
    }
}