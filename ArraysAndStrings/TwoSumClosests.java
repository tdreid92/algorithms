/**
Time: O(nlogn)
Space: O(1)
Arrays.sort is quicksort at nlogn, which is the worst complexity in this algorithm. The while
loop is O(n) because it moves through the collection once.
**/
public class Solution {

  public List<int[]> twoSumClosest (int[] nums, int target) {
      List<int[]> result = new ArrayList<int[]>();
  
      if (nums.length < 2) return result;

      Arrays.sort(nums);
      target -= 30;

      int lo = 0, hi = nums.length - 1, diff = Integer.MAX_VALUE, bestDiff = diff;
      
      while (lo < hi) {
      
        int sum = nums[lo] + nums[hi];
        diff = target - sum;

        if (diff >= 0) {
          if (diff == bestDiff) result.add(new int[]{nums[lo], nums[hi]});
          if (diff < bestDiff) {
            bestDiff = diff;
            result.clear();
            result.add(new int[]{nums[lo], nums[hi]});
          }
          lo++;  
        } else hi--;
        
      }
      
      return result;
  }
  
}