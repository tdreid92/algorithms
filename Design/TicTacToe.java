/**
Design a Tic-tac-toe game that is played between two players on a n x n grid.

You may assume the following rules:

A move is guaranteed to be valid and is placed on an empty block.
Once a winning condition is reached, no more moves is allowed.
A player who succeeds in placing n of their marks in a horizontal, vertical, or diagonal row wins the game.
Example:
Given n = 3, assume that player 1 is "X" and player 2 is "O" in the board.

TicTacToe toe = new TicTacToe(3);

toe.move(0, 0, 1); -> Returns 0 (no one wins)
|X| | |
| | | |    // Player 1 makes a move at (0, 0).
| | | |

toe.move(0, 2, 2); -> Returns 0 (no one wins)
|X| |O|
| | | |    // Player 2 makes a move at (0, 2).
| | | |

toe.move(2, 2, 1); -> Returns 0 (no one wins)
|X| |O|
| | | |    // Player 1 makes a move at (2, 2).
| | |X|

toe.move(1, 1, 2); -> Returns 0 (no one wins)
|X| |O|
| |O| |    // Player 2 makes a move at (1, 1).
| | |X|

toe.move(2, 0, 1); -> Returns 0 (no one wins)
|X| |O|
| |O| |    // Player 1 makes a move at (2, 0).
|X| |X|

toe.move(1, 0, 2); -> Returns 0 (no one wins)
|X| |O|
|O|O| |    // Player 2 makes a move at (1, 0).
|X| |X|

toe.move(2, 1, 1); -> Returns 1 (player 1 wins)
|X| |O|
|O|O| |    // Player 1 makes a move at (2, 1).
|X|X|X|

Personal solution. I realize I can optimize the memory by using just Arrays for each user, rather than a 2-D array.
I have not yet gotten to implementing the solution that way.
**/
class TicTacToe {
    int[][] board;
    int n;
    int count;

    /** Initialize your data structure here. */
    public TicTacToe(int n) {
        board = new int[n][n];
        this.n = n;
        count = 0;
    }
    
    /** Player {player} makes a move at ({row}, {col}).
        @param row The row of the board.
        @param col The column of the board.
        @param player The player, can be either 1 or 2.
        @return The current winning condition, can be either:
                0: No one wins.
                1: Player 1 wins.
                2: Player 2 wins. */
    public int move(int row, int col, int player) {
        count++;
        board[row][col] = player;
        if (count >= n - 1) {
            if (isRowWon(row, player)) return player;
            if (isColWon(col, player)) return player;
            if (isDiagWon(player)) return player;
        }
        return 0;
    }
    
    private boolean isRowWon(int row, int player) {
        for (int i = 0; i < n; i++) {
            if (board[row][i] != player) return false;
        }
        return true;
    }
    
    private boolean isColWon(int col, int player) {
        for (int i = 0; i < n; i++) {
            if (board[i][col] != player) return false;
        }
        return true;
    }
    
    private boolean isDiagWon(int player) {
        if (isDiagCross1Won(player)) return true;
        if (isDiagCross2Won(player)) return true;
        return false;
    }
    
    //iterate top left to bottom right
    private boolean isDiagCross1Won(int player) {
        for (int i = 0; i < n; i++) {
            if (board[i][i] != player) return false;
        }
        return true;
    }
    
    //iterate bottom left to top right
    private boolean isDiagCross2Won(int player) {
        for (int i = n - 1; i >= 0; i--) {
            if (board[i][n - i - 1] != player) return false;
        }
        return true;
    }
}

/**
 * Your TicTacToe object will be instantiated and called as such:
 * TicTacToe obj = new TicTacToe(n);
 * int param_1 = obj.move(row,col,player);
 */