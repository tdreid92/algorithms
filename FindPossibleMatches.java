/**
Given a dictionary of words (there, their, ball, bath, and, are), search words for pattern ba** 
where * can be replaced with only single character between a-z, for example, for given pattern ba**, 
program should return bath, ball

SOLVED BY SELF!!!!! Used backtracking template
Time O(26^n) where n is the length of word
Space O(m) where m is the size of the dictionary
**/
class Solution {

  public List<String> find(Set<String> wordDict, String word) {
    List<String> list = new ArrayList<String>();
    backtrack(list, wordDict, "", 0, word);
    return list;
  }

  public void backtrack(List<String> list, Set<String> wordDict, String temp, int start, String word) {
      for (int i = start; i < word.length(); i++) {
        if (word.charAt(i) == '*') {
          for (char c = 'a'; c <= 'z'; c++) {
            backtrack(list, wordDict, temp += c, i + 1, word);
            temp = temp.substring(0, temp.length() - 1);
          }
        } else {
          temp += word.charAt(i);
        }
      }
    if (temp.length() == word.length() && wordDict.contains(temp)) list.add(temp);
  }