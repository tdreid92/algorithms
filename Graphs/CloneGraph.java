/*
Given a reference of a node in a connected undirected graph, return a deep copy (clone) of the graph. Each node in the graph contains a val (int) and a list (List[Node]) of its neighbors.
// Definition for a Node.
class Node {
    public int val;
    public List<Node> neighbors;

    public Node() {}

    public Node(int _val,List<Node> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
};
Time: O(n)
Space: O(n)
Solution by self. Depth first search, similar approach to CopyRandomPointer problem. Kept track
of visited nodes with map.
*/

class Solution {
    Map<Node, Node> map = new HashMap<Node, Node>(); 
    public Node cloneGraph(Node node) {
        return clone(node);
    }
    
    public Node clone(Node node) {
        if (node == null) return null;
        if (map.containsKey(node)) return map.get(node);
        
        List<Node> neighbors = new ArrayList<Node>();
        Node newNode = new Node(node.val, neighbors);
        map.put(node, newNode);
        for (Node thisNeighbor : node.neighbors) {
            neighbors.add(clone(thisNeighbor));
        }

        return newNode;
    }
}