/**
Space: O(mn)
Time: O(mn)
Solved by self. iterate through grid, avoiding O's until you find shortest amount of steps required to get to X.
Simple BFS.
**/
public class Solution { 
  public static final int[][] dirs = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
  
  public static int getShortestDistance(char[][] grid) {
  
    Queue<int[]> q = new LinkedList<int[]>();
    int[][] visited = new int[grid.length][grid[0].length];
    
    int[] currPos;
    int maxY = grid.length;
    int maxX = grid[0].length;
    visited[0][0] = 1;
    int steps = 0;

    q.offer(new int[]{0,0});
    while (!q.isEmpty()) {
      int sz = q.size();
      for (int i = 0; i < sz; i++) {
        currPos = q.poll();
        int y = currPos[0];
        int x = currPos[1];
        if (grid[y][x] == 'X') return steps;
        for (int[] dir : dirs) {
          int nr = currPos[1] + dir[1];
          int nc = currPos[0] + dir[0];
          if (nr >= 0 && nr < maxX && nc >= 0 && nc < maxY && grid[nc][nr] != 'O' &&
            visited[nc][nr] != 1) {
            q.offer(new int[]{nc, nr});
            visited[nc][nr] = 1;
          }
        }
        add(q, x, y, visited, grid);
      }
      steps++;
    }
    return -1;
  }
}