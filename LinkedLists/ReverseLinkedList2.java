/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 Time: O(n)
 Space: O(1)
 CREDIT TO ardyadipta for this amazing solution. Studied the "pre, start, then" combo with a whiteboard. It's a fing
 amazing reversal algorithm. Start and then progress through while pre stays the same and pre.next becomes the tail.
 Its pretty cool visually. Anyway, dummyhead again.
 */
class Solution {
    public ListNode reverseBetween(ListNode head, int m, int n) {
        ListNode dummy = new ListNode(-1);
        dummy.next = head;
        ListNode pre = dummy;
        
        for (int i = 0; i < m - 1; i++) pre = pre.next;
        
        ListNode start = pre.next;
        ListNode then = start.next;
        
        for (int i = 0; i < n - m; i++) {
            start.next = then.next;
            then.next = pre.next;
            pre.next = then;
            then = start.next;
        }
            
        return dummy.next;
    }
}