# README #

This is a one-stop repository for all my algorithm solutions and related analyses. 

# NOTE #

I DO NOT TAKE FULL CREDIT FOR ALL THESE SOLUTIONS. Many of these solutions are either inspired by other users' solutions
after optimization of my original solution. Some are directly from them as well. I give credit when possible for the
users. Of course, I heavily analyzed and studied their solutions and keep them here as they are my favorite answers to
my favorite problems!