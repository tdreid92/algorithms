/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
/**
Todo: figure out time and space complexity
**/
class Solution {
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> result = new LinkedList<List<Integer>>();
        if (root == null) return result;
        
        Queue<TreeNode> q = new LinkedList<TreeNode>();
        q.offer(root);
        while (q.size() > 0) {
            int levelSize = q.size();
            List<Integer> innerList = new LinkedList<Integer>();
            for (int i = 0; i < levelSize; i++) {
                if (q.peek().left != null) q.offer(q.peek().left);
                if (q.peek().right != null) q.offer(q.peek().right);
                innerList.add(q.poll().val);
            }
            result.add(innerList);
        }
        return result;
    }
}