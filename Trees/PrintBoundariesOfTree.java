/**Given a binary tree, return the values of its boundary in anti-clockwise direction starting from root. Boundary includes left boundary, leaves, and right boundary in order without duplicate nodes.  (The values of the nodes may still be duplicates.)

Left boundary is defined as the path from root to the left-most node. Right boundary is defined as the path from root to the right-most node. If the root doesn't have left subtree or right subtree, then the root itself is left boundary or right boundary. Note this definition only applies to the input binary tree, and not applies to any subtrees.

The left-most node is defined as a leaf node you could reach when you always firstly travel to the left subtree if exists. If not, travel to the right subtree. Repeat until you reach a leaf node.

The right-most node is also defined by the same way with left and right exchanged.

Example 1

Input:
  1
   \
    2
   / \
  3   4

Ouput:
[1, 3, 4, 2]

Explanation:
The root doesn't have left subtree, so the root itself is left boundary.
The leaves are node 3 and 4.
The right boundary are node 1,2,4. Note the anti-clockwise direction means you should output reversed right boundary.
So order them in anti-clockwise without duplicates and we have [1,3,4,2].
**/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 Time: O(n)
 Space: O(n)
 Traverse all left boundaries first, then get left leaves, then right leaves, then traverse right boundaries, 
 but add to list in reverse fashion of left boundaries method. GetLeaves only adds when there are no leaf
 children. left/right Boundary method only adds if children exist and root has some child. Credit to andyreadsall
 for helping me structure code and remove need for hashset
 */
class Solution {
    public List<Integer> list = new ArrayList<Integer>();
    public List<Integer> boundaryOfBinaryTree(TreeNode root) {
        if (root == null) return list;
        list.add(root.val);
        getLeftBoundaries(root.left);
        getLeaves(root.left);
        getLeaves(root.right);
        getRightBoundaries(root.right);
        return list;
    }
    
    public void getLeftBoundaries (TreeNode root) {
        if (root == null || (root.left == null && root.right == null)) return;
        list.add(root.val);
        if (root.left == null) getLeftBoundaries(root.right);
        else getLeftBoundaries(root.left);
    }
       
    public void getRightBoundaries (TreeNode root) {
        if (root == null || (root.left == null && root.right == null)) return;
        if (root.right == null) getRightBoundaries(root.left);
        else getRightBoundaries(root.right);
        list.add(root.val);
    }
    
    public void getLeaves (TreeNode root) {
        if (root == null) return;
        if (root.left == null && root.right == null) list.add(root.val);
        else {
            getLeaves(root.left);
            getLeaves(root.right);
        }
    }
}