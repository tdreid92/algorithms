/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
/**
Time: O(n)
Space: O(n)
**/
class Solution {
    public boolean isSubtree(TreeNode s, TreeNode t) {
        if (s == null || t == null) return false;
        if (s.val == t.val) {
            if (validateSubTree(s, t)) return true;
        }
        return isSubtree(s.left, t) || isSubtree(s.right, t);
    }
    
    private boolean validateSubTree(TreeNode s, TreeNode t) {
        if ((s == null && t != null) || (t == null && s != null)) return false;
        if ((s == null && t == null)) return true;
        if (s.val != t.val) return false;
        return validateSubTree(s.left, t.left) && validateSubTree(s.right, t.right);
    }
}